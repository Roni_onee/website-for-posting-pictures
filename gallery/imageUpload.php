<?php
session_start();

if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
    // no session, redirect user to index.php
	header('location: ../index.php');
}

include('../private/config.php');
require('../private/db_config.php');

if(isset($_POST) && !empty($_FILES['image']['name']) && !empty($_POST['title'])){

	$name = $_FILES['image']['name'];
	list($txt, $ext) = explode(".", $name);
	$image_name = time().".".$ext;
	$tmp = $_FILES['image']['tmp_name']; // used to upload image in folder


	if(move_uploaded_file($tmp, 'uploads/'.$image_name)){

		$sql = "INSERT INTO image_gallery (title, image) VALUES ('".$_POST['title']."', '".$image_name."')";

		$result = $db_conection->query($sql);

        if($result)
        {
        	$_SESSION['success'] = 'Image Uploaded successfully.';
		    header("Location: ./index.php"); // used for redirection

        }
        else{
        	$_SESSION['error'] = 'image uploading failed';
		    header("Location: ./index.php");
        }
	}else{
		$_SESSION['error'] = 'image uploading failed';
		header("Location: ./index.php");
	}
}else{
	$_SESSION['error'] = 'Please Select Image or Write title';
	header("Location: ./index.php");
}

?>