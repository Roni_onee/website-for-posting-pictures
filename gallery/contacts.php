<?php
//include auth_session.php file on all user panel pages
include('../private/autoload.php');
session_start();

if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
    // no session, redirect user to index.php
    header('location: ../index.php');
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>Image Gallery</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- References: https://github.com/fancyapps/fancyBox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

    <style type="text/css">
        .contact-us-form input, img{
            display: block;
            font-size:11px;
            padding:4px 2px;
            border:solid 1px #aacfe4;
            width:200px;
            margin:2px 0 20px 10px;
        }
        .gallery {
            display: inline-block;
            margin-top: 20px;
        }

        .close-icon {
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }

        .form-image-upload {
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }

        .carousel-inner>.item>a>img,
        .carousel-inner>.item>img,
        .img-responsive,
        .thumbnail a>img,
        .thumbnail>img {
            width: 300px !important;
            height: 160px !important;
        }
    </style>
</head>

<body>
<?php include '../includes/navbar.php'?>

    <div class="container">

        <div class="row">
        <form method="POST" action="send_message.php" class="contact-us-form">
                        <label for="name">Name:*</label>
                        <input name="name" type="text" id="name" required></input>
                        <label for="contact_email">Email:*</label>
                        <input name="contact_email" type="email" id="contact_email" required></input>
                        <label for="message">Message:*</label>
                        <br>
                        <textarea name="message" rows="15" id="message" style="width: 50em;" required ></textarea>
                        <label for="captcha" id="enterCode"></label>
              			<img src="captcha.php" alt="CAPTCHA" class="captcha-image">
                        <br>
                        <input type="text" id="captcha" name="captcha_challenge" placeholder="Please enter the code form picture" pattern="[A-Z]{6}">
                        <button class="fas fa-redo refresh-captcha"> Reset captcha</button>
              			<br>
                        <button type="submit" class="button">SEND</button>
            </form>

        </div> <!-- row / end -->
    </div> <!-- container / end -->
</body>
</html>

<script type="text/javascript">
                            var refreshButton = document.querySelector(".refresh-captcha");
                                refreshButton.onclick = function() {
                            document.querySelector(".captcha-image").src = 'captcha.php?' + Date.now();}
</script>


<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none",
        });
    });
</script>