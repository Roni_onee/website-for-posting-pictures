<?php
session_name('gallary');
session_start();

if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
    // no session, redirect user to index.php
	header('location: ../index.php');
}

include('../private/config.php');
require('../private/db_config.php');


if(isset($_POST) && !empty($_POST['id'])){

	   // select image to delete    
	   $sql_select = "SELECT image FROM image_gallery WHERE id = ".$_POST['id'];
	   $select_result = $db_conection->query($sql_select);
	    $row = $select_result->fetch_row();
		$image_name = $row[0];

		// code to unlink(delete)  image physically from folder 
		$unl = unlink("./uploads/".$image_name);

		$sql = "DELETE FROM image_gallery WHERE id = ".$_POST['id'];
		$db_conection->query($sql);


		$_SESSION['success'] = 'Image Deleted successfully.';
		header("Location: ./index.php");
}else{
	$_SESSION['error'] = 'Please Select Image or Write title';
	header("Location: ./index.php");
}


?>