<?php

if($_POST) {
   $name = "";
   $contact_email = "";
   $email_title = "Contact Form";
   $message = "";


   if(isset($_POST['captcha_challenge']) && $_POST['captcha_challenge'] == $_SESSION['captcha_text']) {

       if(isset($_POST['name'])) {
           $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
       }

       if(isset($_POST['contact_email'])) {
           $contact_email = str_replace(array("\r", "\n", "%0a", "%0d"), '', $_POST['contact_email']);
           $contact_email = filter_var($contact_email, FILTER_VALIDATE_EMAIL);

       }



       if(isset($_POST['message'])) {
           $message = htmlspecialchars($_POST['message']);
       }



      $recipient = "mail@ribnikov.eu";


      $headers  = array(
            "From: {$name} . {$contact_email}",
            "MIME-Version: 1.0",
            "Content-Type: text/html;charset=utf-8"
        );

       if(mail($recipient, $email_title, $message, implode("\r\n", $headers))) {
           $message = "Thank you for the message we will contact you within 24 hours.";
       } else {
           $message="Sorry, the message could not be sent.";
       }
   } else {
       $message = "You were input wrong code.";
   }

} else {
   $message= "Something goes wrong.";
}

echo "<script>
alert('$message');
window.location.href='/index.html';
</script>";
?>