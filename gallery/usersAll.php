<?php
//include auth_session.php file on all user panel pages
include('../private/autoload.php');
session_start();

if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
    // no session, redirect user to index.php
    header('location: ../index.php');
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>Image Gallery</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- References: https://github.com/fancyapps/fancyBox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

    <style type="text/css">
        .gallery {
            display: inline-block;
            margin-top: 20px;
        }

        .close-icon {
            border-radius: 50%;
            position: absolute;
            right: 5px;
            top: -10px;
            padding: 5px 8px;
        }

        .form-image-upload {
            background: #e8e8e8 none repeat scroll 0 0;
            padding: 15px;
        }

        .carousel-inner>.item>a>img,
        .carousel-inner>.item>img,
        .img-responsive,
        .thumbnail a>img,
        .thumbnail>img {
            width: 300px !important;
            height: 160px !important;
        }
        th{
            
        }
    </style>
</head>

<body>
<?php include '../includes/navbar.php'?>

    <div class="container">

        <div class="row">
        <div style="text-align: center;">
            <h2>Below are the all registered users</h2>
                <p></p>
        </div>
            <div class='list-group gallery' style="width:100%;">
            <table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Full Name</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
                <?php
                require('../private/autoload.php');

                $sql = "SELECT * FROM users";
               if($users = $db_conection->query($sql)){
                while ($user = $users->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>".$user['id']."</td>";
                    echo "<td>".$user['username']."</td>";
                    echo "<td>".$user['email']."</td>";
                    echo "</tr>";
                }
                }?>

            </div> <!-- list-group / end -->
        </div> <!-- row / end -->
    </div> <!-- container / end -->
</body>
</html>